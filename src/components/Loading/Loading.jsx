import React,{Component} from  'react'
import './Loading.scss'

export default class Loading extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <section style={{display:(this.props.show?'':'none')}} className='waiting'>
                <em><i/></em>
            </section>
    }
};