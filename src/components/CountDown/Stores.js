import Actions from './Actions'
import Reflux,{createStore} from 'reflux'
import DB from '../../app/db'
import assign from 'lodash/assign'

export default createStore({
    
    listenables: [Actions],

    sendRegistCode(message){
        DB.Regist.sendMessage(message).then(data=>{
            this.data.djs = true;
            $('#beforeSendCode').hide();
            this.trigger(this.data);
        },data=>{
            assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        });
    },

    sendBindvalidateCode(message){
        DB.Banks.sendBindvalidateCode(message).then(data=>{
            this.data.djs = true;
            this.trigger(this.data);
        }, data=>{
            assign(this.data,data);
            this.data.modalIsOpen = true;
            this.trigger(this.data);
        })
    },

    closeModal(){
        this.data.modalIsOpen = false;
        this.trigger(this.data);
    },

    getInitialState() {
        this.data = {
            modalIsOpen:false,
            errorMsg:'',
            djs:false,
        };
        return this.data;
    }
});