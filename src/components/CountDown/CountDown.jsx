// import React,{Component} from  'react'

// import ReactMixin from 'react-mixin'
// import Reflux from 'reflux'
// import Actions from './Actions'
// import store from './Stores'
// import Cookies from '../../components/Cookie'
// import Modal from 'react-modal'

// import './CountDown.scss'

// export default class CountDown extends Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//         	codeTxt:'发送验证码',
//             modalIsOpen:false,
//             run:false,
//             runNing:false
//         }
//     }

//     componentWillUnmount() {
//         clearInterval(this.sm);
//     }

//     openModal(){
//         this.setState({modalIsOpen:true});
//     }

//     closeModal() {
//         this.setState({modalIsOpen:false});
//         Actions.closeModal();
//     }

//     sendCode(){
//         if(this.state.run)return;
//         this.props.click();
//         this.setState({
//             runNing:false
//         });
//     }

//     componentDidUpdate() {
//         if(this.props.sendNow&&!this.state.run&&!this.state.runNing){
//             let time = this.props.time||60;
//             this.setState({
//                 codeTxt:`(${time}s)重新发送`,
//                 run:true,
//                 runNing:true,
//             });
//             this.sm = setInterval(()=>{
//                 if(time === 1){
//                     clearInterval(this.sm);
//                     this.setState({
//                         codeTxt:`重发验证码`,
//                         run:false,
//                     });
//                     return;
//                 }
//                 this.setState({
//                     codeTxt:`(${--time}s)重新发送`
//                 });
//                 this.props.resetCode();
//             },1000);
//         }
//     }

//     render() {
//         const st = this.state.m;
//         return <a id='countDown' className={this.state.run?'off':''} href="javascript:;"
//         ref='codeTxt' onClick={this.sendCode.bind(this)}>
//             {this.state.codeTxt}
//             <Modal
//                 isOpen={st.modalIsOpen||this.state.modalIsOpen}
//                 onAfterOpen={this.openModal.bind(this)}
//                 onRequestClose={this.closeModal.bind(this)}
//                 closeTimeoutMS={50}
//             >
//                 <div className='content'>
//                     <i onClick={this.closeModal.bind(this)}>&times;</i>
//                     {st.modalIsOpen?st.errorMsg:this.props.false}
//                 </div>
//                 <div className='btn' onClick={this.closeModal.bind(this)}>
//                     <a href="javascript:;">我知道了</a>
//                 </div>
//             </Modal>
//         </a>
//     }
// };

// ReactMixin.onClass(CountDown, Reflux.connect(store,'m'));