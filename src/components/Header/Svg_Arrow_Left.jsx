import React,{Component} from  'react'

export default class Svg extends Component {
    render() {
        const fill = this.props.fill || '#ccc';
        // const fill = '#ccc';
        return <svg viewBox="200 0 370 1024" enableBackground="new 0 0 1024 1024">
                <path className="svgpath" fill={fill} d="M362.4585 486.9818 748.0193 873.143 701.0671 920.0932 315.3035 534.3467 315.3035 534.3467 270.0073 489.0524 713.77 45.3083 759.0662 90.6026Z"/>
            </svg>
    }
};