import Arrow_Left from './Svg_Arrow_Left.jsx'
import React,{Component} from 'react'
import _assign from 'lodash/assign'
import './Header.scss'
import { RouteTransition } from 'react-router-transition';
import { connect } from 'react-redux'

class Header extends Component {

    back(backBtn){
        if(window.history.length <= 1){
            location.hash = '/#';
            return;
        }
        
        if(typeof(backBtn) === 'string'){
            if(backBtn === 'mzlicai://close'){
                location.href = 'mzlicai://close';
                setTimeout(()=>{
                    if(location.pathname === '/inapp/'){//http://yzd.mizlicai.com/inapp/
                        location.href=`//h5.mizlicai.com/#/assets`;
                    }else if(location.pathname !== '/'){
                        location.href=`${location.origin}/#/assets`
                    }
                },200);
            }else{
                location.hash = backBtn;
            }
        }else if(typeof(backBtn) === "function"){
            backBtn();
        }else if(history.length < 2){
            location.hash = '/';
        }else{
            history.back(-1);
        }
    }

    render() {
        const myTitle = this.props.myTitle;
        const rightBtn = myTitle.rightBtn;
        const back_dom = myTitle.backBtn ? (<div onClick={this.back.bind(this,myTitle.backBtn)}>
                        <i id="return"></i>
                        <div></div>
                    </div>) : <a/>;

        let right_btn;
        if(rightBtn){
            if(rightBtn[2]){
                right_btn = <i onClick={()=>location.hash = rightBtn[1]} id={rightBtn[2]}></i>
            }else{
                right_btn = <a className='top_right' href="javascript:;" onClick={()=>location.hash = rightBtn[1]}>{rightBtn[0]}</a>
            }
        }
        myTitle.title&&(document.title=myTitle.title);
        return this.props.show?<RouteTransition
                    pathname={location.hash}
                    atEnter={{ opacity: 0 }}
                    atLeave={{ opacity: 0 }}
                    atActive={{ opacity: 1 }}
                  >
                <header ref='header' className="header">
                <div className="left">
                    {back_dom}
                </div>
                <div id="js_header_title" className="title">{myTitle.title}</div>
                <div>
                    {right_btn}
                </div>
                </header>
            </RouteTransition>:null
    }
};

function select(state) {
    return {
        myTitle:state.setTitle,
    }
}

export default connect(select)(Header);