import DBF from './dbFactory'

export default DBF.context;

DBF.create('Loan', {
    getLoanList : {
        url       : '/licai/loans/user',
    },
    getLoanDetail : {
        url       : '/loans/detail',
    },
    getRepaymentPlans : {
        url       : '/licai/loans/repaymentplan',
    },
    calculateApplyLoan : {
        url       : '/licai/loans/calculateapplyloan',
    },
    apply:{
        url       : '/licai/loans/apply',
        type      : 'POST'
    },
});

DBF.create('Home', {
    homes : {
        url       : '/licai/users/home',
    },
    mizlicailogin: {
        url       : '/licai/users/mizlicailogin',
        type      : 'POST'
    },
});

DBF.create('RepayMent', {
    getRecord : {
        url       : '/licai/loanbills/repayments',
    },
    outstandingloan : {
        url       : '/licai/loanbills/outstandingloan',
    },
    outstanding: {
        url       : '/licai/loanbills/outstanding',
    },
    repayment: {
        url       : '/licai/loanbills/repayment',
        type      : 'POST'
    },
    calculateprepaymentamount:{
        url       : '/licai/loanbills/calculateprepaymentamount',
    },
    prepayments:{
        url       : '/licai/loanbills/prepayments',
    },
    detail:{
        url       : '/licai/loanbills/detail',
    },
    cashierPay:{
        url       : '/cashierPay',
        type      : 'POST'
    },
});

DBF.create('Personal', {
    getUserMsg : {
        url       : '/users',
    },
});

DBF.create('Bank', {
    getBankList : {
        url       : '/bankinfos',
    },
    getUserauths : {
        url       : '/userauth',
    },
    getBankCard : {
        url       : '/licai/bankcards/user',
    },
    sendBindvalidateCode: {
        url       : '/bankcards/sendbindvalidatecode',
        type      : 'POST'
    },
    confirm: {
        url       : '/bankcards/confirm',
        type      : 'POST'
    },
    isbankcardbind: {
        url       : '/bankcards/isbankcardbind',
    },
    cashierAuth:{
        url       : '/cashierAuth',
        type      : 'POST'
    }
});