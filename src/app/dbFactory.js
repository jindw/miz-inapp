import Cookies from '../components/Cookie'
import _assign from 'lodash/assign'
import _map from 'lodash/map'
import _includes from 'lodash/includes'
import * as actions from './actions.jsx'

export default {
    __ : {},
    set(key, value) {
        this.__[key] = value;
    },
    get(key) {
        return this.__[key];
    },
    create(name, methods) {
        this.context[name] = new DB(methods);
    },
    context : {
        link: data => this.context.Data = data,
        Data: {}
    }
}

let urlPrefix;
if (__LOCAL__) {
    urlPrefix = '//121.40.107.224:8080';
} else {
    urlPrefix = '//sc.mizlicai.com';
}

// function DB(methods) {
//     _map(methods,(config,method) => this[method] = query => new request(config,query));
// }
class DB {
    constructor(methods) {
        _map(methods, (config, method) => this[method] = query => new Request(config, query, method));
    }
}
class Request {
    constructor(config, querys, method) {
        const mergeQuery = {
            os: 'h5',
            userKey: Cookies.getCookie('userKey')
        };

        return new Promise((resolve, reject) => {
            $.ajax({
                url: `${urlPrefix}${config.url}.json`,
                type: config.type,
                data:Object.assign({},mergeQuery,querys),
                // data: _assign(mergeQuery, querys),
                timeout: 15000,
                cache: false,
                beforeSend() {
                    window.__store__.dispatch(actions.setLoadingState(true))
                }
            }).done(resp => {
                if (!resp.status) {
                    window.__store__.dispatch(actions.Responce(method, resp));
                    resolve(resp);
                } else {
                    if (resp.status === 1017) {
                        Cookies.clearCookie('userKey');
                    }
                    swal({
                        text: resp.errorMsg,
                        confirmButtonText: '确定',
                        timer: resp.status === 1017
                            ? ''
                            : 2000,
                        confirmButtonColor: '#fff',
                        allowOutsideClick: false
                    }).then(() => {
                        if (resp.status === 1017) {
                            if (_includes(location.href, 'os=') || (localStorage.os && localStorage.os !== 'false')) {
                                location.href = 'mzlicai://close';
                            } else {
                                history.back(-1);
                            }
                        } else if (resp.status === 1000) {
                            history.back(-1);
                        }
                    });
                }
            }).fail(() => {
                swal({text: '请求超时,请稍后再试！', confirmButtonText: '确定', allowOutsideClick: false, confirmButtonColor: '#fff'});
            }).always(() => window.__store__.dispatch(actions.setLoadingState(false)));
        })
    }
}
