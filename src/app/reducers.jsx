import {combineReducers} from 'redux'
import * as actions from './actions.jsx'
import DB from './db'
import _assign from 'lodash/assign'

function setTitle(state = [], action) {
    switch (action.type) {
        case actions.Title:
            // console.log(action)
            // console.log(Reflect.get(action, 'title'))
            // alert(1111111111111111111111111)
            // return Reflect.get(action, 'title');
            return action.title || {};
        default:
            return state;
    }
}

function showLoading(state = [], action) {
    switch (action.type) {
        case actions.SET_Loading_State:
            return action.show || false;
        default:
            return state;
    }
}

function Responce(state = [], action) {
    const res = actions.RESPONCE;
    switch (action.type) {
        case res.GETBANKCARD:
            return _assign({}, state, {
                [res.GETBANKCARD]: action.resp
            });
        case res.HOMES:
            return _assign({}, state, {
                [res.HOMES]: action.resp
            });
        case res.ISBANKCARDBIND:
            return _assign({}, state, {
                [res.ISBANKCARDBIND]: action.resp
            });
            // const isBind = action.resp.isBind;
            // if(isBind){
            //     location.hash=`#/loan`;
            // }else{
            //     DB.Bank.cashierAuth();
            // }
            // return state;
        case res.CASHIERAUTH:
            // const data = action.resp;
            return _assign({}, state, {
                [res.ISBANKCARDBIND]: action.resp
            });
            // if(data.url){
            //     location.href=data.url;
            // }else{
            //     swal({
            //         text: data.errorMsg,
            //         confirmButtonText: '确定',
            //         confirmButtonColor: '#fff',
            //         allowOutsideClick:false,
            //     }).done();
            // }
            // return state;
        case res.OUTSTANDINGLOAN:
            return _assign({}, state, {
                [res.OUTSTANDINGLOAN]: action.resp
            });
        case res.GETRECORD:
            return _assign({}, state, {
                [res.GETRECORD]: action.resp
            });
        case res.GETREPAYMENTPLANS:
            return _assign({}, state, {
                [res.GETREPAYMENTPLANS]: action.resp
            });
        case res.GETUSERMSG:
            return _assign({}, state, {
                [res.GETUSERMSG]: action.resp
            });
        case res.CALCULATEAPPLYLOAN:
            return _assign({}, state, {
                [res.CALCULATEAPPLYLOAN]: action.resp
            });
        case res.APPLY:
            return _assign({}, state, {
                [res.APPLY]: action.resp
            });
        case res.GETLOANLIST:
            return _assign({}, state, {
                [res.GETLOANLIST]: action.resp
            });
        case res.GETLOANDETAIL:
            return _assign({}, state, {
                [res.GETLOANDETAIL]: action.resp
            });
        case res.OUTSTANDING:
            return _assign({}, state, {
                [res.OUTSTANDING]: action.resp
            });
        case res.DETAIL:
            return _assign({}, state, {
                [res.DETAIL]: action.resp
            });
        case res.PREPAYMENTS:
            return _assign({}, state, {
                [res.PREPAYMENTS]: action.resp
            });
        case res.CALCULATEPREPAYMENTAMOUNT:
            return _assign({}, state, {
                [res.CALCULATEPREPAYMENTAMOUNT]: action.resp
            });
        case res.CASHIERPAY:
            return _assign({}, state, {
                [res.CASHIERPAY]: action.resp
            });
        case res.MIZLICAILOGIN:
            return _assign({}, state, {
                [res.MIZLICAILOGIN]: action.resp
            });
        default:
            return state;
    }
}

const todoApp = combineReducers({setTitle, showLoading, Responce})

export default todoApp
