import React, {Component} from 'react'
import {connect} from 'react-redux'
import _includes from 'lodash/includes'
import Cookies from '../components/Cookie'
// import { HashRouter, Match, Miss } from 'react-router'
import Header from '../components/Header'
import Home from '../pages/Home'
import Loan from '../pages/Loan'
import LoanDetail from '../pages/LoanDetail'
import Repayment from '../pages/Repayment'
import Faq from '../pages/Faq'
import Loading from '../components/Loading'
import DB from './db'
import * as actions from './actions.jsx'

import {HashRouter, Route, Switch} from 'react-router-dom'

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            minHeight: 0
        }
        const mizLicaiUserId = this.getUrlParam('id');
        const os = _includes(location.search, 'os=');
        localStorage.os = os;
        if (Cookies.getCookie('userKey') === 'undefined') {
            Cookies.clearCookie('userKey');
        }
        if (mizLicaiUserId) {
            Cookies.clearCookie('userKey');
            DB.Home.mizlicailogin({mizLicaiUserId});
            // else{
            //     Cookies.setCookie('userKey','14385_d2b336bc802a4c13bfbed5eace9aa500',1);
            //     return;
            // }
        } else if (!Cookies.getCookie('userKey')) {
            window.__store__.dispatch(actions.setLoadingState(false));
            swal({text: '请使用正确的方式进入', confirmButtonText: '确定', allowOutsideClick: false, confirmButtonColor: '#fff'}).then(() => {
                if (os) {
                    location.href = 'mzlicai://close';
                } else if (history.length > 2) {
                    history.back(-1);
                } else {
                    location.reload();
                }
            });
        }
    }

    getUrlParam(key) {
        const hash = location.hash;
        const arr = !hash
            ? []
            : location.hash.substr(hash.indexOf('?') + 1).split('&');
        const param = {};
        for (let i = 0, l = arr.length; i < l; i++) {
            const kv = arr[i].split('=');
            param[kv[0]] = kv[1];
        }
        return key
            ? (param[key] || '')
            : param;
    }

    getAppversion() {
        const app = navigator.appVersion;
        const ios = 'mizlicai_iOS';
        const and = 'mizlicai_Android';
        const ios_start = app.indexOf(ios);
        const and_start = app.indexOf(and);
        if (ios_start !== -1) { //ios
            return gets(ios, ios_start);
        } else if (and_start !== -1) {
            return gets(and, and_start);
        }

        function gets(version, start) {
            const end = app.substring(start).indexOf(')');
            return app.substring(start + version.length + 1, start + end);
        }
    }

    componentDidMount() {
        $(window).on('resize', () => this.resize()).trigger('resize');
    }

    resize() {
        this.setState({
            minHeight: $(window).height() - ($('header').height() || 0)
        });
    }

    componentDidUpdate() {
        $('header').css('position', 'fixed');
    }

    render() {
        const version = this.getAppversion();
        let show = false;

        const iphone = navigator.appVersion.indexOf('iPhone') !== -1;
        // alert('version:'+version)
        if (iphone
            ? version >= 2.81
            : version >= 297) {
            show = false;
        } else {
            show = true;
        }

        if (navigator.userAgent.toLowerCase().match(/MicroMessenger/i) == "micromessenger") {
            show = false;
        }
        // show = false;
        window.__show__ = show;
        return <HashRouter>
            <section>
                <Header show={show}/>
                <div className="body" style={{
                    minHeight: this.state.minHeight,
                    marginTop: show
                        ? ''
                        : 0
                }}>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/loan" component={Loan}/>
                        <Route path="/loan/:type" component={Loan}/>
                        <Route exact path="/loanDetail" component={LoanDetail}/>
                        <Route path="/loanDetail/:loanid" component={LoanDetail}/>
                        <Route path="/loanDetail/:loanid/:plan" component={LoanDetail}/>

                        <Route exact path="/repayment" component={Repayment}/>
                        <Route exact path="/repayment/:type" component={Repayment}/>
                        <Route exact path="/repayment/ahead/:id" component={Repayment}/>
                        <Route path="/repayment/recorddetail/:loanid/:type" component={Repayment}/>

                        <Route path="/faq" component={Faq}/>
                        <Route component={Home}/>
                    </Switch>
                </div>
                <Loading show={this.props.showLoading}/>
            </section>
        </HashRouter>
    }
}

function select(state) {
    return {showLoading: state.showLoading}
}

export default connect(select)(App);
