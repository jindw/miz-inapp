"use strict";
//基本组件
import React, {Component} from 'react'
import {render} from 'react-dom'

import './whole.scss'
// import '../../node_modules/sweetalert2/dist/sweetalert2.css'

import {Provider} from 'react-redux'
import {createStore} from 'redux'
import todoApp from './reducers.jsx'
let stores = createStore(todoApp);

window.__store__ = stores;

document.title = '米庄贷';

import App from './Apps.jsx'

render(
    <Provider store={stores}>
    <App/>
</Provider>, document.getElementById('app'));
