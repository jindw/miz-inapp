export const Title = 'Title'

export function setTitle(title) {
    return {type: Title, title}
}

export const SET_Loading_State = 'SET_Loading_State'

export function setLoadingState(show) {
    return {type: SET_Loading_State, show}
}

export const RESPONCE = {
    GETBANKCARD: 'GETBANKCARD',
    HOMES: 'HOMES',
    ISBANKCARDBIND: 'ISBANKCARDBIND',
    CASHIERAUTH: 'CASHIERAUTH',
    OUTSTANDINGLOAN: 'OUTSTANDINGLOAN',
    GETRECORD: 'GETRECORD',
    GETREPAYMENTPLANS: 'GETREPAYMENTPLANS',
    GETUSERMSG: 'GETUSERMSG',
    CALCULATEAPPLYLOAN: 'CALCULATEAPPLYLOAN',
    APPLY: 'APPLY',
    GETLOANLIST: 'GETLOANLIST',
    GETLOANDETAIL: 'GETLOANDETAIL',
    OUTSTANDING: 'OUTSTANDING',
    DETAIL: 'DETAIL',
    PREPAYMENTS: 'PREPAYMENTS',
    CALCULATEPREPAYMENTAMOUNT: 'CALCULATEPREPAYMENTAMOUNT',
    CASHIERPAY: 'CASHIERPAY',
    MIZLICAILOGIN: 'MIZLICAILOGIN'
}

export function Responce(method, resp) {
    const mtd = method.toUpperCase();
    return {type: RESPONCE[mtd], resp}
}
