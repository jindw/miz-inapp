/**
 * 首页
 */
import React, {Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import Cookies from '../../components/Cookie'
import _includes from 'lodash/includes'
import ModalTip from '../../components/ModalTip'

import DB from '../../app/db'

import './Home.scss'
import {Motion, spring} from 'react-motion'

import { connect } from 'react-redux'
import * as actions from '../../app/actions.jsx'

class Home extends Component {

    constructor(props) {
        super(props);
        this.state ={
            height:$(window).height(),
            textIndent:0,
            tipValue:'2017-01-26至2017-02-02期间，米庄贷暂停借款申请，自2017-02-03恢复正常，请用户知悉。',
        }
        this.showTip = this.showTip.bind(this);
    }

    home(type=true){
        let home;
        if(type){
            home = setInterval(()=>{
                const userKey = this.props.MIZLICAILOGIN.userKey||Cookies.getCookie('userKey');
                if(userKey){
                    Cookies.setCookie('userKey', userKey, 1);
                    DB.Home.homes();
                    DB.Bank.getBankCard();
                    clearInterval(home);
                }
            }, 100);
        }else{
            clearInterval(home);
        }
    }

    componentWillUnmount() {
        this.home(false);
    }

    componentDidMount(){
        this.props.dispatch(actions.setTitle({
            title: '米庄贷',
            backBtn:'mzlicai://close',
        }));
        this.home();
        this.showTip();
    }

    showTip(){
        const span = $('#myTip span');
        if(span.length){
            if(-span.text().length * $(window).width()*0.035 > (this.state.textIndent - span.width())){
                if($('#myTip span label').length > 1){
                    this.setState(prevState=> ({
                        textIndent: prevState.textIndent + $('#myTip span label').width()+200
                    }));
                }else{
                    span.append(`<label>${this.state.tipValue}</label>`);
                }
            }else{
                this.setState(prevState=>({
                    textIndent:--prevState.textIndent
                }));
            }
            requestAnimationFrame(this.showTip);
        }
    }

    getLocalNum(num){
        const [i, re, len] = [`${num}`.indexOf('.'), (+num).toFixed(2).toLocaleString(), `${num}`.length];
        return len - i === 2&&i!==-1?`${re}0`:re;
    }

    render() {
        const st = this.state.m;
        const home = this.props.HOMES;
        return <section id='home'>
            <div id='top'>
                <p>可借款金额(元)</p>
                <Motion defaultStyle={{x: 0}} style={{x: spring(home.availableAmount||0)}}>
                    {value => <label>
                        {this.getLocalNum(value.x)}
                        </label>
                    }
                </Motion>
                <Motion defaultStyle={{x: 0}} style={{x: spring(home.quotaAmount||0)}}>
                    {value => <span>总额度:{this.getLocalNum(value.x)}元</span>}
                </Motion>
                <dl>
                    <dd>
                        <div>
                            7日内应还款(元)
                            <Motion defaultStyle={{x: 0}} style={{x: spring(home.day7ShouldRepaymentAmount||0)}}>
                                {value => <span>{this.getLocalNum(value.x)}</span>}
                            </Motion>
                        </div>
                    </dd>
                    <dd><div>
                            全部应还款(元)
                            <Motion defaultStyle={{x: 0}} style={{x: spring(home.totalShouldRepaymentAmount||0)}}>
                                {value => <span>{this.getLocalNum(value.x)}</span>}
                            </Motion>
                        </div>
                    </dd>
                </dl>
            </div>
            <section style={{display:'none'}} id="myTip"
                onClick={()=>this.setState({tipOutMessage:this.state.tipValue})}>
                <i/>
                <span style={{textIndent:this.state.textIndent}}>
                    <label>{this.state.tipValue}</label>
                </span>
            </section>

            <div id="container">
                <dl className='rightSvg'>
                    <dd id='xinyong'>信用提额 <label>(暂未开通)</label></dd>
                    <dd id='jiekuan' onClick={()=>location.hash='#/loandetail'}>借款记录<Arrow fill='#c7c7cc'/></dd>
                    <dd id='about' onClick={()=>location.hash='#/faq'}>关于米庄贷
                    <Arrow fill='#c7c7cc'/></dd>
                </dl>
            </div>

            <footer>
                <dl>
                    <dd onClick={()=>location.hash = '#/repayment'}>还款</dd>
                    <dd onClick={()=>{
                        const bankCard = this.props.GETBANKCARD.bankCard;
                        const bankCardId = bankCard&&bankCard.id;
                        if(!bankCardId){
                            swal({
                                text: '请重新登录',
                                confirmButtonText: '确定',
                                timer:2000,
                                confirmButtonColor: '#fff',
                            }).then(()=>{
                               location.href = 'mzlicai://close';
                               setTimeout(()=>{
                                   if(location.pathname === '/inapp/'){
                                       location.href=`//h5.mizlicai.com/#/assets`;
                                   }else if(location.pathname !== '/'){
                                       location.href=`${location.origin}/#/assets`
                                   }
                               }, 200);
                            });
                        }else{
                            DB.Bank.isbankcardbind({bankCardId})
                             .then(data=>{
                                if(data.isBind){
                                    location.hash=`#/loan`;
                                }else{
                                    DB.Bank.cashierAuth().then(data=>{
                                        if(data.url){
                                            location.href=data.url;
                                        }else{
                                            swal({
                                                text: data.errorMsg,
                                                confirmButtonText: '确定',
                                                confirmButtonColor: '#fff',
                                                allowOutsideClick:false,
                                            }).done();
                                        }
                                    });
                                }
                            })
                        }
                    }}>借款</dd>
                </dl>
            </footer>
            <ModalTip msg={this.state.tipOutMessage} showTip={!!this.state.tipOutMessage}
                close={()=>this.setState({tipOutMessage:''})}
            />
        </section>
   }
}

function select(state) {
    return {
        HOMES:state.Responce.HOMES||{},
        GETBANKCARD:state.Responce.GETBANKCARD||{},
        ISBANKCARDBIND:state.Responce.ISBANKCARDBIND||{},
        MIZLICAILOGIN:state.Responce.MIZLICAILOGIN||{},
    }
}
export default connect(select)(Home);
