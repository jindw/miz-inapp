/**
 * 借款
 */
import React, {Component} from 'react'
import Modal from 'react-modal'
import ModalPsd from 'react-modal'
import Cookies from '../../components/Cookie'
import _times from 'lodash/times'
import LoanAgreeMent from './LoanAgreeMent.jsx'
import ModalAgreeMent from 'react-modal'
import classNames from 'classnames'
import ModalTip from '../../components/ModalTip'

import './Loan.scss'

import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'
import DB from '../../app/db'
import {Motion, spring} from 'react-motion'

class Loan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errmsg: '',
            psdlen: 0,
            firstpsd: '',
            cssPlan: '',
            lilv: 0,
            termType: 0,
            modalPsdIsOpen: false,
            modalPsdIsOpenWait: false,
            mypsd: '',
            agree: true,
            loanValue: '-',
            modalAgreeMentIsOpen: false
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '申请借款', backBtn: true}));
        DB.Bank.getBankCard();
        DB.Loan.getRepaymentPlans();
        DB.Home.homes();
        DB.Personal.getUserMsg();
    }

    openModalAgreeMent() {
        this.setState({modalAgreeMentIsOpen: true});
        this.props.dispatch(actions.setTitle({
            title: '借款协议',
            backBtn: () => {
                this.closeModalAgreeMent();
            }
        }));
    }

    closeModalAgreeMent() {
        this.setState({modalAgreeMentIsOpen: false});
        this.componentDidMount();
    }

    openModalPsd() {
        const psd = this.state.mypsd;
        this.refs.password.value = psd;
        this.inPassword();
    }

    closeModalPsd() {
        this.setState({modalPsdIsOpen: false, psdlen: 0, mypsd: '', firstpsd: ''});
    }

    inPassword() {
        this.refs.password.focus();
    }

    psdKey() {
        const password = this.refs.password;
        const mypsd = password.value.replace(/[^0-9]/g, "");
        password.value = mypsd;
        this.setState({psdlen: mypsd.length, mypsd});
        if (mypsd.length === 6) {
            // this.psdNext();
            this.setState({modalPsdIsOpen: false});
            DB.Loan.apply({amount: this.state.loanValue, payPassword: this.refs.password.value, bankCardId: this.props.GETBANKCARD.bankCard.id, repaymentPlanId: this.props.GETREPAYMENTPLANS.repaymentPlan.id})
            this.closeModalPsd();
        }
    }

    psdNext() {
        const payPassword = this.refs.password.value;
        if (payPassword.length < 6) {
            swal({text: '请输入6位数字密码', confirmButtonText: '确定', timer: 2000, confirmButtonColor: '#fff'}).done();
        }
        // else{
        //     this.setState({
        //         modalPsdIsOpen: false,
        //     });
        //     DB.Loan.apply({
        //         amount:this.state.loanValue,
        //         payPassword,
        //         bankCardId:this.props.GETBANKCARD.bankCard.id,
        //         repaymentPlanId:this.props.GETREPAYMENTPLANS.repaymentPlan.id,
        //     })
        // }
    }

    toNext() {
        const [loanValue,
            agree] = [this.refs.val.value, this.state.agree];
        let err;
        if (loanValue < 1000) {
            err = '请输入正确的借款金额(1000以上的整数)';
        } else if (this.props.HOMES.availableAmount < loanValue) {
            err = '申请借款金额超过可借款金额';
        } else if (!agree) {
            err = '请阅读并同意《米庄贷用户借款协议》';
        }

        if (err) {
            swal({text: err, confirmButtonText: '确定', timer: 2000, confirmButtonColor: '#fff'}).done();
        } else {
            this.setState({modalPsdIsOpen: true, loanValue});
        }
    }

    loanInMoney(e) {
        const val = e.target;
        val.value = val.value.replace(/^0+|[^0-9]/g, "");
        this.setState({val: val.value});
        if (val.value) {
            DB.Loan.calculateApplyLoan({loanAmount: val.value, repaymentPlanId: this.props.GETREPAYMENTPLANS.repaymentPlan.id})
        } else {
            this.props.dispatch(actions.Responce('calculateApplyLoan', {}));
        }
    }

    toAgree() {
        this.setState(prevState => ({
            agree: !prevState.agree
        }));
    }

    render() {
        const psdMod = [];
        const bank = this.props.GETBANKCARD.bankCard || {};
        const calcu = this.props.CALCULATEAPPLYLOAN;
        _times(6, (ind) => psdMod.push(
            <dd key={`psdMod_${ind}`} className={classNames({
                on: this.state.psdlen > ind
            })}>
                <i/></dd>
        ));
        return <section className='loan'>
            <dl className='rightSvg'>
                <dd className='hasinput'>借款金额
                    <input maxLength='7' onChange={this.loanInMoney.bind(this)} ref='val' placeholder='请输入1000以上的整数' type="tel"/>
                    <span>元</span>
                </dd>
                <dd>每日利息
                    <Motion defaultStyle={{
                        x: 0
                    }} style={{
                        x: spring(calcu.dayInterest || 0)
                    }}>
                        {value => <span className='red'>{value.x.toFixed(2)}
                            <label>元</label>
                        </span>}
                    </Motion>
                </dd>
                <dd>总利息<em>(默认30天)</em>
                    <Motion defaultStyle={{
                        x: 0
                    }} style={{
                        x: spring(calcu.interest || 0)
                    }}>
                        {value => <span className='red'>{value.x.toFixed(2)}
                            <label>元</label>
                        </span>}
                    </Motion>
                </dd>
                <dd>服务费
                    <Motion defaultStyle={{
                        x: 0
                    }} style={{
                        x: spring(calcu.serviceFee || 0)
                    }}>
                        {value => <span>{value.x.toFixed(2)}
                            <label>元</label>
                        </span>}
                    </Motion>
                </dd>
                <dd>到期应还款<em>(默认30天)</em>
                    <Motion defaultStyle={{
                        x: 0
                    }} style={{
                        x: spring(calcu.expectedAmount || 0)
                    }}>
                        {value => <span className='red'>{value.x.toFixed(2)}
                            <label>元</label>
                        </span>}
                    </Motion>
                </dd>
                <dd>收款银行<span ref='myBankName'>
                        {bank.bankName || '-'}(尾号{(bank.cardNumberOriginal || '****').substring((bank.cardNumberOriginal || '****').length - 4)})
                    </span>
                </dd>
            </dl>
            <p id='rule'>最长30天，随借随还，日息0.035%，服务费{calcu.serviceFee || 0}元/笔</p>
            <p id='box_check'>
                <i onClick={this.toAgree.bind(this)} className={classNames({on: this.state.agree})}></i>
                我已阅读并同意
                <a href="javascript:;" onClick={this.openModalAgreeMent.bind(this)}>
                    《米庄贷用户借款协议》</a>
            </p>
            <a id='submit' onClick={this.toNext.bind(this)} href="javascript:;">下一步</a>

            <ModalPsd isOpen={this.state.modalPsdIsOpen} onAfterOpen={this.openModalPsd.bind(this)} onRequestClose={this.closeModalPsd.bind(this)} closeTimeoutMS={150} style={{
                content: {
                    overflow: 'hidden'
                }
            }}>
                <div className='content' id='password'>
                    <i className='timesX' onClick={this.closeModalPsd.bind(this)}>&times;</i>
                    请输入您的交易密码
                </div>
                <div id='main'>
                    <p>借款金额：<label>￥{this.state.loanValue}</label>
                    </p>
                    <dl className='password_confirm' onClick={this.inPassword.bind(this)}>
                        {psdMod}
                    </dl>
                    <input autoFocus onKeyUp={this.psdKey.bind(this)} ref='password' maxLength="6" type="tel"/>
                    <a id='submit' style={{
                        backgroundColor: (this.state.psdlen === 6
                            ? ''
                            : "#ccc")
                    }} onClick={this.psdNext.bind(this)} href="javascript:;">确定</a>
                </div>
            </ModalPsd>
            <ModalAgreeMent isOpen={this.state.modalAgreeMentIsOpen} onAfterOpen={this.openModalAgreeMent.bind(this)} onRequestClose={this.closeModalAgreeMent.bind(this)} closeTimeoutMS={150} style={{
                overlay: {
                    top: $('header').height(),
                    height: $(window).height() - ($('header').height() || 0),
                    overflowY: 'scroll'
                },
                content: {
                    width: '100%',
                    overflowY: 'scroll',
                    position: 'relative'
                }
            }}>
                <section onClick={this.closeModalAgreeMent.bind(this)}>
                    <LoanAgreeMent {...this.props.GETUSERMSG}/>
                </section>
            </ModalAgreeMent>
            <ModalTip title='提交成功' msg='您申请的借款将于审核通过后一个工作日内打款至您的银行卡' showTip={this.props.APPLY.status === 0} close={() => {
                this.props.dispatch(actions.Responce('apply', {}));
                location.replace('#/loandetail');
            }}/>
        </section>
    }
};

function select(state) {
    return {
        GETREPAYMENTPLANS: state.Responce.GETREPAYMENTPLANS || {},
        GETUSERMSG: state.Responce.GETUSERMSG || {},
        GETBANKCARD: state.Responce.GETBANKCARD || {},
        CALCULATEAPPLYLOAN: state.Responce.CALCULATEAPPLYLOAN || {},
        HOMES: state.Responce.HOMES || {},
        APPLY: state.Responce.APPLY || {},
        ISBANKCARDBIND: state.Responce.ISBANKCARDBIND || {}
    }
}
export default connect(select)(Loan);
