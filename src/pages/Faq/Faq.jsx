/**
 * 问答页面
 */
import React, {Component} from 'react'
import _times from 'lodash/times'

import './Faq.scss'

import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'

class Faq extends Component {
    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '关于米庄贷', backBtn: true}));

        const heights = [];
        _times($('#faq dl').length, ind => heights.push($('dd').eq(ind).height()));
        this.setState({heights});
        this.props.dispatch(actions.setLoadingState(false));
    }

    openDetail(e) {
        const next = e.target.nextSibling;
        const bf = $(`#${this.state.openId}`);
        bf.animate({
            height: 0,
            padding: 0
        }, 300, () => bf.css('display', 'none'));

        this.setState({openId: null});

        if (next.style.display === 'block')
            return;

        $(next).css({height: 0, display: 'block'});

        $(next).animate({
            height: this.state.heights[next.id],
            padding: '10px 0'
        }, 300, () => $(next).css('height', 'auto'));
        this.setState({openId: next.id});
    }

    render() {
        return <section id='faq'>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>1. 米庄贷如何申请？</dt>
                <dd id='0'>答：用户购买一月期、二月期、三月期、半年期、一年期等普通定期理财产品，且理财资金到期兑付时间大于35天，符合期限要求的理财金额不少于10000元的情况下，即可申请。
                    <br/>注：米庄贷申请成功时，用户持有的理财产品将不可进行转让或提前赎回。
                </dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>2. 米庄贷额度如何计算？</dt>
                <dd id='1'>答：最大可借款额度=符合期限要求的理财金额*0.95<br/>
                    米庄贷额度会根据用户理财产品到期时间变动等情况做实时调整，同时每位用户最高可申请借款的额度不超过20万元人民币（如下表）：
                    <ul>
                        <li className='start'></li>
                        <li>符合要求的<br/>理财资金&lt;1万</li>
                        <li>1万&le;符合要求的理财资金&le;21万</li>
                        <li>符合要求的<br/>理财资金&gt;21万</li>

                        <li className='start'>最大可借款<br/>额度（元）</li>
                        <li className='red'>0</li>
                        <li className='red'>符合要求的<br/>理财资金*0.95</li>
                        <li className='red'>200,000</li>
                    </ul>

                    若用户理财产品到期兑付时间不足35天，且在米庄理财APP上无其余符合条件的理财产品，则用户米庄贷的可借款额度为零，同时用户仍可在米庄贷界面查看历史借款记录和还款记录。
                </dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>3. 米庄贷如何计息？</dt>
                <dd id='2'>答：米庄贷采取按日计息，随借随还的计息模式。每日利率为0.035%（万3.5）。从放款成功当日开始计息，计息至用户还款成功当日。</dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>4.米庄贷可以借款多久？</dt>
                <dd id='3'>答：米庄贷属于按日计息，随借随还的借款产品，最长的借款时间为30天（自然日）。</dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>5. 米庄贷可以申请几次？</dt>
                <dd id='4'>答：额度范围内，用户申请米庄贷借款的次数不受限制。</dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>6. 米庄贷如何放款与还款？</dt>
                <dd id='5'>答：申请借款将在一个工作日内直接打款至用户在米庄理财APP绑定的银行卡；<br/>
                    关于还款，用户可以选择通过银行卡发起主动还款，还款请求发出后，米庄贷会从用户指定银行卡中扣除相应金额； 也可以在30个自然日借款期满之后保证绑定银行卡中有足够资金，由米庄贷发起扣款。<br/>
                    若用户借款到期后未还款，米庄贷还款资金（包含本息及逾期费用）将会在用户的理财资金到期兑付时，直接从理财资金中扣除。</dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>7.米庄贷逾期费用如何计算？</dt>
                <dd id="6">答：逾期费用从用户逾期第一个自然日开始收取，逾期费用的计算方式为逾期金额的0.1%（千分之一）。</dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>8.还款失败、部分还款成功时如何计算利息？</dt>
                <dd id="7">答：由于用户卡内余额不足导致还款失败或部分还款成功，如未及时补足卡内资金并重新提交还款申请，造成还款延误所产生的利息由用户自行承担，利息按照全额应还本金计算。</dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>9.米庄贷服务费如何计算？</dt>
                <dd id="8">答：服务费为第三方支付渠道费用，按照单次借款每笔4元收取，具体标准将根据实际情况做相应调整。</dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>10.米庄贷借款申请提交后，可否撤回？</dt>
                <dd id="9">答：用户的借款申请一旦提交成功，不可撤回。</dd>
            </dl>
            <dl>
                <dt onClick={this.openDetail.bind(this)}>
                    <label>11.</label>
                    米庄贷借款因银行卡限额问题还款失败，如何处理？
                </dt>
                <dd id="10">答：方案①，用户可在还款界面新增（选定）另一张限额较大的银行卡用于还款；<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;方案②,需用户通过线下转账方式还款至我公司账户，并在还款时联系客服做相关及后续处理。我公司收款账户具体如下：

                    <ul className='bank'>
                        <li>户名</li>
                        <li>杭州信釜资产管理有限公司</li>

                        <li>账号</li>
                        <li>571908266810112</li>

                        <li>开户银行</li>
                        <li>招商银行杭州之江支行</li>
                    </ul>

                </dd>
            </dl>
        </section>
    }
};

export default connect()(Faq);
