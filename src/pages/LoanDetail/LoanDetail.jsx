import React, {Component} from 'react'
import Main from './Main.jsx'
import List from './List.jsx'

import './Plan.scss'

export default class LoanDetail extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const match = this.props.match;
        if (match.params.loanid) {
            return <Main {...match}/>
        }
        return <List/>
    }
}
