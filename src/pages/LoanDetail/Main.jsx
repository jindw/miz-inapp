/**
 * 借款详情
 */
import React, {Component} from 'react'
import Cookies from '../../components/Cookie'
import Loading from '../../components/Loading'
import _includes from 'lodash/includes'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'

import './Main.scss'
import {Motion, spring} from 'react-motion'

import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'
import DB from '../../app/db'

class Main extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '借款详情', backBtn: true}));
        DB.Loan.getLoanDetail({loanNumber: this.props.params.loanid});
    }

    render() {
        const loan = this.props.GETLOANDETAIL.loan;
        const createDate = new Date(loan.createDate || 0);
        const returnDate = new Date(loan.divideSettlementDate || 0);
        const realPayDate = new Date(loan.realPayDate || 0);
        const simpleLoanStatus = loan.simpleLoanStatus === '已拒绝'
            ? '未通过'
            : loan.simpleLoanStatus;
        const dt = new Date(returnDate.getFullYear(), returnDate.getMonth(), returnDate.getDate());
        return <section className='loandetail'>
            <div className='me rightSvg'>
                <div className='top'>
                    总金额（元）
                    <label>按时还款利息以30日计算</label>
                    <Motion defaultStyle={{
                        x: 0
                    }} style={{
                        x: spring(loan.expectRepayAmount || 0)
                    }}>
                        {value => <p>{value.x.toFixed(2)}</p>}
                    </Motion>
                    <ul>
                        <li>
                            <label>本金（元）</label>
                            <Motion defaultStyle={{
                                x: 0
                            }} style={{
                                x: spring(loan.amount || 0)
                            }}>
                                {value => <span>{value.x.toFixed(2)}</span>}
                            </Motion>
                        </li>
                        <li>
                            <label>日利率</label>
                            <Motion defaultStyle={{
                                x: 0
                            }} style={{
                                x: spring(loan.loanRatio * 100 || 0)
                            }}>
                                {value =>< span > {
                                    (value.x).toFixed(3)
                                } % (万分之 {
                                    (value.x * 100).toFixed(1)
                                }) < /span>}
                            </Motion>
                        </li>
                    </ul>
                </div>
                <dl>
                    <dd style={{
                        display: (loan.createDate
                            ? ''
                            : 'none')
                    }}>申请时间<span>{dateFormat(createDate, "yyyy-mm-dd HH:MM:ss")}</span>
                    </dd>
                    <dd style={{
                        display: (simpleLoanStatus
                            ? ''
                            : 'none')
                    }}>借款状态<span>{simpleLoanStatus}</span>
                    </dd>
                </dl>
                <dl style={{
                    display: (loan.hasPayAction
                        ? ''
                        : 'none')
                }}>
                    <dd>打款时间
                        <span>{dateFormat(realPayDate, "yyyy-mm-dd HH:MM:ss")}</span>
                    </dd>
                    <dd style={{
                        display: (loan.payOff
                            ? ''
                            : 'none')
                    }}>还款状态
                        <span className='done'>
                            已结清
                        </span>
                    </dd>
                    <dd style={{
                        display: (loan.payOff || !loan.loanBillId || new Date() > dt
                            ? 'none'
                            : '')
                    }} onClick={() => {
                        sessionStorage.ids = loan.loanBillId;
                        location.hash = '#/repayment/ahead/confirm';
                    }}>提前还款
                        <Arrow fill='#c7c7cc'/>
                    </dd>
                </dl>
            </div>
        </section>
    }
};

// ReactMixin.onClass(Main, Reflux.connect(store,'m'));

function select(state) {
    return {
        GETLOANDETAIL: state.Responce.GETLOANDETAIL || {
            loan : {}
        }
    }
}
export default connect(select)(Main);
