/**
 * 借款列表
 */
import React, {Component} from 'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'

import './List.scss'
import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'
import DB from '../../app/db'

class List extends Component {

    constructor(props) {
        super(props);
    }

    getStatus() {
        return {
            STATUS_LOANSUBMIT: '审核中',
            STATUS_APPROVEPASS: '打款中',
            STATUS_LOANREMITING: '打款中',
            STATUS_LOANFAIL: '打款中',
            STATUS_LOANSUCC: '打款成功',
            STATUS_LOANREJECTED: '未通过',
            STATUS_APPROVEREJECTED: '未通过',
            STATUS_ISLOANING: '打款中',
            STATUS_LOANPASS: '审核中',
            STATUS_APPROVEWAIT: '审核中'
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '借款记录', backBtn: true}));
        DB.Loan.getLoanList();
    }

    render() {
        const loans = [];
        const list = this.props.GETLOANLIST;
        list.loans.forEach((itm, ind) => {
            const dt = new Date(itm.createDate);
            loans.push(
                <dd key={`loans_${ind}`} onClick={() => location.hash = `#/loanDetail/${itm.loanNumber}`}>
                    <span>
                        {(itm.amount).toFixed(2)}
                        <i style={{
                            display: (itm.payOff
                                ? ''
                                : 'none')
                        }}>已结清</i>
                        <Arrow fill='#c7c7cc'/>
                    </span>
                    <label>{`借款日:${dt.getFullYear()}-${dt.getMonth() + 1}-${dt.getDate()}`}</label>
                    <em className={itm.loanStatus}>
                        {this.getStatus()[itm.loanStatus]}
                    </em>
                </dd>
            );
        });
        return <section className='loan_list'>
            <dl id={list.status === 0 && !list.loans.length
                ? 'list_null'
                : ''} className='rightSvg'>
                {loans}
            </dl>
        </section>
    }
};

// ReactMixin.onClass(List, Reflux.connect(store,'m'));
function select(state) {
    return {
        GETLOANLIST: state.Responce.GETLOANLIST || {
            loans : []
        }
    }
}
export default connect(select)(List);
