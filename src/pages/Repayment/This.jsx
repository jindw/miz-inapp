import React,{Component} from  'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'

import './This.scss'

import { connect } from 'react-redux'
import * as actions from '../../app/actions.jsx'
import DB from '../../app/db'

class This extends Component {

	constructor(props) {
        super(props);
        // Actions.outstanding();
    }

    componentDidMount(){
        this.props.dispatch(actions.setTitle({
             title: '还款账单',
            backBtn:true,
            rightBtn:['还款记录','#/repayment/record']
        }));
        DB.RepayMent.outstanding();
        // __event__.setHeader({
        //     title: '还款账单',
        //     backBtn:true,
        //     rightBtn:['还款记录','#/repayment/record']
        // });
    }

    // componentWillUnmount() {
    //      __event__.setHeader({
    //         title: '本期还款',
    //         backBtn:true,
    //         rightBtn:false
    //     });
    // }

    showAll(list){
    	const myList = $(this.refs[list]);
    	let myHeight = myList.find('dt').height();
    	if(!myList.hasClass('showing')){
    		const shows = $('.showing');
    		if(shows){
    			shows.toggleClass('showing')
    			shows.animate({
		    		height: myHeight
		    	},300);
    		}
    		myHeight = myList.find('dd').length * myList.find('dd').height() + myList.find('dt').height();
    	}

    	myList.toggleClass('showing');
    	myList.animate({
    		height: myHeight
    	},300);
    }

	render() {
		const loanBills = [];
        const out = this.props.OUTSTANDING;
		out.loanBills.forEach((itm,ind)=>{
			const settlementDate = new Date(itm.settlementDate);

			let lists = [];

			itm.details.forEach((itms,ind)=>{
				const rdt = new Date(itms.loan.valueDate);
				lists.push(<dd key={`lists_${ind}`}>
					<span>{rdt.getMonth()+1}月{rdt.getDate()}日</span>
					<div>借款{itms.loan.amount.toFixed(2)}元</div>
					<label>本期还款<em>{itms.divideRepayAmount}</em>元</label>
				</dd>);
			});

			loanBills.push(<dl key={`loanBills_${ind}`} className='list_return' ref={`list_${ind}`}>
				<dt className='rightSvg' onClick={this.showAll.bind(this,`list_${ind}`)}>
					<label>还款日</label>
					<div>{settlementDate.getMonth()+1}月{settlementDate.getDate()}日</div>
					<p>{itm.totAmount}<em>含利息{(itm.interest).toFixed(2)}元 服务费{(itm.serviceFee).toFixed(2)}元</em></p>
					<Arrow fill='#ccc'/>
				</dt>
				{lists}
			</dl>)
		});

		return <section id='repayment_this'>
			{loanBills}
            <dl id={out.status === 0 && !out.loanBills.length?'list_null':''}></dl>
		</section>
    }
};

// ReactMixin.onClass(This, Reflux.connect(store,'m'));
function select(state) {
    return {
        OUTSTANDING:state.Responce.OUTSTANDING||{
            loanBills:[]
        },
    }
}
export default connect(select)(This);