/**
 * 提前还款
 */
import React, {Component} from 'react'
import _map from 'lodash/map'
import DB from '../../app/db'

import './Ahead.scss'

import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'

class Ahead extends Component {

    constructor(props) {
        super(props);
        this.state = {
            totAmount: 0
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '自主还款', backBtn: true}));
        DB.RepayMent.prepayments();
        DB.Bank.getBankCard();
    }

    _Selects(r, e) {
        if ($(this.refs[r]).hasClass('on')) {
            this.refs[r].className = '';
        } else {
            this.refs[r].className = 'on';
        }
        let totAmount = 0,
            loans = 0,
            ids = [];
        _map($('.on'), (itm, ind) => {
            totAmount += + itm.getAttribute('data-loans');
            loans += + itm.getAttribute('data-loans');
            ids.push(itm.id);
        });
        this.setState({totAmount, loans, ids});
    }

    onSubmit() {
        if (this.state.totAmount) {
            sessionStorage.ids = this.state.ids;
            location.hash = '#/repayment/ahead/confirm';
        }
    }

    render() {
        const loanBills = this.props.PREPAYMENTS.loanBills;
        const list = [];
        loanBills.forEach((itm, ind) => {
            const inDate = new Date(itm.details[0].loan.valueDate);
            list.push(
                <dd className={itm.status} id={itm.id} key={`loanBills_${ind}`} ref={`m_${ind}`} data-totamount={itm.totAmount} data-loans={itm.totAmount - itm.interest} onClick={this._Selects.bind(this, `m_${ind}`)}>
                    <p className={itm.status}>
                        {dateFormat(inDate, "yyyy/mm/dd")}
                        <label>{itm.statusName}</label>
                    </p>
                    <div ref={`m_${ind}`} className='number'>借款本金(元)<span>{((itm.totAmount - itm.interest).toFixed(2))}</span>
                        <i style={{
                            display: 'block'
                        }}/>
                    </div>

                </dd>
            );
        });
        return <section className='repayment_ahead'>
            <dl id={this.props.PREPAYMENTS.status === 0 && !loanBills.length
                ? 'list_null'
                : ''}>
                <dt style={{
                    display: (loanBills.length
                        ? ''
                        : 'none')
                }}>共{loanBills.length}笔借款未还清</dt>
                {list}
            </dl>
            <footer style={{
                display: (loanBills.length
                    ? ''
                    : 'none')
            }}>
                <div>共<em>{this.state.totAmount?(this.state.totAmount).toFixed(2):'--'}</em>元</div>
                <a href="javascript:;" style={{
                    backgroundColor: (this.state.totAmount
                        ? ''
                        : '#999')
                }} onClick={this.onSubmit.bind(this)}>还款</a>
            </footer>
        </section>
    }
}

function select(state) {
    return {
        PREPAYMENTS: state.Responce.PREPAYMENTS || {
            loanBills : []
        }
    }
}
export default connect(select)(Ahead);
