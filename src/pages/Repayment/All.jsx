/**
 * 未还款综合
 */
import React, {Component} from 'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'

import './All.scss'
import {Motion, spring} from 'react-motion'

import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'
import DB from '../../app/db'
import {Link} from 'react-router-dom'

class All extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({
            title: '还款',
            backBtn: true,
            rightBtn: ['还款记录', '#/repayment/record']
        }));
        DB.RepayMent.outstandingloan();
    }

    render() {
        const osloan = this.props.OUTSTANDINGLOAN;
        return <section className='repayment_all'>
            <div className='detail'>
                <Motion defaultStyle={{
                    x: 0
                }} style={{
                    x: spring(osloan.outstandingLoanPrincipal || 0)
                }}>
                    {value => <p>{value.x.toFixed(2)}</p>}
                </Motion>
                <span>共<em>{osloan.outstandingLoanCount}</em>笔借款未结清</span>
            </div>
            <dl className='rightSvg list_return'>
                <dt onClick={() => location.hash = '#/repayment/this'}>
                    <span>按期还款</span>
                    <label>本期<em>{(osloan.outstandingBillPrincipal|| 0).toFixed(2) }元</em>
                    </label>
                    <Arrow/>
                </dt>
                <dd onClick={() => location.hash = '#/repayment/ahead'}>
                    <span>自主还款</span>
                    <label>共<em>{osloan.prepaymentLoanBillCount}</em>笔</label>
                    <Arrow/>
                </dd>
            </dl>
            <Link id='torecord' to="repayment/record" style={{
                display: (__show__
                    ? 'none'
                    : '')
            }}>查看还款记录</Link>

        </section>
    }
}

function select(state) {
    return {
        OUTSTANDINGLOAN: state.Responce.OUTSTANDINGLOAN || {}
    }
}
export default connect(select)(All);
