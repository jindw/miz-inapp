import React, {Component} from 'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'
import './Record.scss'

import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'
import DB from '../../app/db'
import _includes from 'lodash/includes'

class Record extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '还款记录', backBtn: true}));
        DB.RepayMent.getRecord();
    }

    render() {
        const record = this.props.GETRECORD;
        const repayments = [];
        let repaymentsYear;

        const billStatus = {
            PRESTATUS_SUCC: '自主还款成功',
            PRESTATUS_FAIL: '自主还款失败',
            PRESTATUS_PARTIALSUCC: '自主还款部分成功',
            SCHSTATUS_SUCC: '自动扣款成功',
            SCHSTATUS_FAIL: '自动扣款失败',
            SCHSTATUS_PARTIALSUCC: '自动扣款部分成功',
            SCHSTATUS_REPAYFAIL: "自动扣款还款失败"
        }

        const over = ['PRESTATUS_SUCC', 'SCHSTATUS_SUCC'];
        const red = ['PRESTATUS_FAIL', 'SCHSTATUS_FAIL', 'SCHSTATUS_REPAYFAIL'];

        record.repayments && record.repayments.forEach((itm, ind) => {
            const dt = new Date(itm.settlementDate);
            if (repaymentsYear !== dt.getFullYear()) {
                repayments.push(
                    <dt key={`repayments_year_${ind}`}>{dt.getFullYear()}年</dt>
                );
                repaymentsYear = dt.getFullYear();
            }
            const myId = itm.loanbillId || itm.cashierPayId || itm.loanBillHistoryId;
            const type = itm.loanbillId
                ? 'loanbillId'
                : (itm.cashierPayId
                    ? 'cashierPayId'
                    : 'loanBillHistoryId');
            const bankCard = itm.bankCard || {};
            repayments.push(
                <dd onClick={() => location.hash = `/repayment/recorddetail/${myId}/${type}`} key={`repayments_${ind}`} className='list_plan'>
                    <p>{`${dt.getFullYear()}-${dt.getMonth() + 1}-${dt.getDate()}`}</p>
                    <label>【还款】按日计息
                        <em>
                            <i className={_includes(red, itm.billStatus)
                                ? 'red'
                                : ''}>{billStatus[itm.billStatus]}</i>
                        </em>
                    </label>

                    <i style={{
                        display: ((_includes(over, itm.billStatus) || !myId)
                            ? ''
                            : 'none')
                    }}></i>
                    <div className='rightSvg'>
                        {(itm.totAmount).toFixed(2)}
                        <span>应还总额(元)</span>
                        <Arrow fill={(itm.billStatus === 'STATUS_REPAID' || !myId)
                            ? '#fff'
                            : '#c7c7cc'}/>
                    </div>
                    <em>
                        <span>扣款银行</span>
                        <span className='logo'><i className={`mybank_logo s_${bankCard.bankCode}`}/>{itm.bankCard && itm.bankCard.bankName || '-'}(尾号：{itm.bankCard && itm.bankCard.tail || '****'})</span>
                    </em>
                </dd>
            );
        });

        return <section className='repayment_record'>
            <dl id={(!record.status) && !repayments.length
                ? 'list_null'
                : ''}>
                {repayments}
            </dl>
        </section>
    }
};

function select(state) {
    return {
        GETRECORD: state.Responce.GETRECORD || {
            repayments : []
        }
    }
}
export default connect(select)(Record);
