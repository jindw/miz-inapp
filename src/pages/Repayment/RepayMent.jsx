import React,{Component} from  'react'
import All from './All.jsx'
import This from './This.jsx'
import Record from './Record.jsx'
import Ahead from './Ahead.jsx'
import AheadConfirm from './AheadConfirm.jsx'
import RecordDetail from './RecordDetail.jsx'

export default class RepayMent extends Component {

	constructor(props) {
        super(props);
    }

	render() {
		const params = this.props.match.params;
		if(params.id){
			return <AheadConfirm/>
		}else if(params.loanid){
			return <RecordDetail {...this.props}/>
		}
		switch(params.type){
			case 'this':
				return <This/>
			case 'record':
				return <Record/>
			case 'ahead':
				return <Ahead/>
			default:
				return <All/>
		}
    }
};
