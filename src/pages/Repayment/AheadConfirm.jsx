import React, {Component} from 'react'
import ModalPsd from 'react-modal'
import _times from 'lodash/times'

import './AheadConfirm.scss'

import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'
import DB from '../../app/db'

class AheadConfirm extends Component {

    constructor(props) {
        super(props);
        if (!sessionStorage.ids) {
            location.hash = '#/';
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '还款', backBtn: true}));
        DB.RepayMent.calculateprepaymentamount({loanBillIds: sessionStorage.ids});
        DB.Bank.getBankCard().then(data => {
            DB.Bank.isbankcardbind({bankCardId: data.bankCard.id})
        });
    }

    _Confirm() {
        if (!this.props.ISBANKCARDBIND.isBind) {
            DB.Bank.cashierAuth().then(data => {
                if (data.url) {
                    location.href = data.url;
                } else {
                    swal({text: data.errorMsg, confirmButtonText: '确定', confirmButtonColor: '#fff', allowOutsideClick: false})
                }
            });
        } else {
            const calcu = this.props.calcu;
            DB.RepayMent.cashierPay({amount: calcu.amount, havingRepayedAmount: calcu.havingRepayedAmount, loanBillIds: sessionStorage.ids}).then(data => {
                if (data.url) {
                    location.href = data.url;
                } else {
                    swal({text: data.errorMsg, confirmButtonText: '确定', confirmButtonColor: '#fff', allowOutsideClick: false, timer: 2000})
                }
            })
        }
    }

    render() {
        const calcu = this.props.calcu;
        let psdMod = [];
        return <section className='repayment_ahead_confirm'>
            <div>还款总额(元)<span className='red'>{calcu.amount}
                    <label>元</label>
                </span>
            </div>
            <p>选择多笔借款时需全部结清</p>
            <div>利息<span>{(calcu.interest || 0).toFixed(2)}
                    <label>元</label>
                </span>
            </div>
            <div style={{
                display: (calcu.havingRepayedAmount
                    ? ''
                    : 'none')
            }}>已还金额<span>{(calcu.havingRepayedAmount || 0).toFixed(2)}
                    <label>元</label>
                </span>
            </div>
            <div>逾期费<span>{(calcu.overdueFee || 0).toFixed(2)}
                    <label>元</label>
                </span>
            </div>
            <div>服务费<span>{(calcu.serviceFee || 0).toFixed(2)}
                    <label>元</label>
                </span>
            </div>
            <div>还款本金<span>{(calcu.principal || 0).toFixed(2)}
                    <label>元</label>
                </span>
            </div>
            <span>服务费为第三方支付渠道费用，一次性收取。</span>
            <span>建议绑定一张单笔限额大于当前还款额的银行卡。</span>
            <a href="javascript:;" onClick={this._Confirm.bind(this)}>确定</a>
        </section>
    }
}

function select(state) {
    return {
        calcu: state.Responce.CALCULATEPREPAYMENTAMOUNT || {},
        ISBANKCARDBIND: state.Responce.ISBANKCARDBIND || {}
    }
}
export default connect(select)(AheadConfirm);
