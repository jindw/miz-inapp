import React, {Component} from 'react'
import Arrow from '../../components/Header/Svg_Arrow_Left.jsx'

import './RecordDetail.scss'

import {connect} from 'react-redux'
import * as actions from '../../app/actions.jsx'
import DB from '../../app/db'
import _includes from 'lodash/includes'

class RecordDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showCashier: '',
            detailHeight: 'auto',
            detailRealHeight: 0
        }
    }

    componentDidMount() {
        this.props.dispatch(actions.setTitle({title: '还款详情', backBtn: true}));
        const params = this.props.match.params;
        DB.RepayMent.detail({
            [params.type]: params.loanid
        })
    }

    render() {
        const detail = this.props.DETAIL;
        const tradeDate = new Date(detail.tradeDate || 0);
        const state = {
            Prepay: '提前还款',
            Schedule: '自动扣款'
        };
        // const Success = detail.billStatus === 'STATUS_SUCC';
        const cashierPay = [];
        (detail.loanbills || []).forEach((itm, ind) => {
            cashierPay.push(
                <li key={`cashier_${ind}`}>
                    <div>
                        借款金额(元)
                        <label>{itm.principal.toFixed(2)}</label>
                        <span>{dateFormat(itm.applyDate, "yyyy-mm-dd")}</span>
                    </div>
                    <div style={{display:(itm.interest?'':'none')}}>
                      <ol>
                        <li>利 息(元)：<span>{(itm.interest||0).toFixed(2)}</span></li>
                        <li>逾期费(元)：<span>{(itm.overdueFee||0).toFixed(2)}</span></li>
                        <li>服务费(元)：<span>{(itm.serviceFee||0).toFixed(2)}</span></li>
                      </ol>
                    </div>
                </li>
            )
        })

        const billStatus = {
            PRESTATUS_SUCC: '自主还款成功',
            PRESTATUS_FAIL: '自主还款失败',
            PRESTATUS_PARTIALSUCC: '自主还款部分成功',
            SCHSTATUS_SUCC: '自动扣款成功',
            SCHSTATUS_FAIL: '自动扣款失败',
            SCHSTATUS_PARTIALSUCC: '自动扣款部分成功',
            SCHSTATUS_REPAYFAIL: "自动扣款还款失败"
        }

        const pre = ['PRESTATUS_SUCC', 'PRESTATUS_FAIL', 'PRESTATUS_PARTIALSUCC'];

        return <section id='record_detail'>
            <dl>
                <dd>
                    <span>还款状态</span>
                    <span>{billStatus[detail.billStatus]}</span>
                    {/*<span
                            id='state'
                            style={{display:(detail.billStatus === 'STATUS_PARTIALSUCC'&&!Success?'':'none')}}>
                            部分还款
                        </span>
                        <span className='red'
                            id='state'
                            style={{display:(detail.billStatus !== 'STATUS_PARTIALSUCC'&&!Success?'':'none')}}>
                            {state[detail.prepayment]}失败
                        </span>
                        <span style={{display:(Success?'':'none')}}>
                            {state[detail.prepayment]}成功
                        </span>*/}
                </dd>
                <dd style={{
                    display: (detail.billStatusMsg
                        ? ''
                        : 'none')
                }}>
                    <span>失败原因</span>
                    <span>{detail.billStatusMsg}</span>
                </dd>
            </dl>

            <dl>
                <dd>
                    <span>未还金额</span>
                    <span>
                        <em id='unpay'>{((detail.totAmount || 0) - (detail.succAmount || 0)).toFixed(2)}</em>元</span>
                </dd>
                <dd>
                    <span>已还金额</span>
                    <span>
                        <em className='paid'>{(detail.succAmount || 0).toFixed(2)}</em>元</span>
                </dd>
                <dd>
                    <span>还款总额</span>
                    <span>
                        <em className='paid'>{(detail.totAmount || 0).toFixed(2)}</em>元</span>
                </dd>
            </dl>

            <dl>
                <dd>
                    <span>流水号</span>
                    <span>{detail.orderNumber}</span>
                </dd>
                <dd style={{
                    display: (detail.tradeDate
                        ? ''
                        : 'none')
                }}>
                    <span>交易时间</span>
                    <span>{dateFormat(tradeDate, "yyyy-mm-dd HH:MM:ss")}</span>
                </dd>
            </dl>

            <dl>
                <dd>
                    <span>还款途径</span>
                    <span><i className={`mybank_logo s_${detail.repayBankCode}`}/>{detail.repayBankName}(尾号{detail.repayBankTail}）</span>
                </dd>
            </dl>

            <dl id='about' className={this.state.showCashier} style={{
                display: (cashierPay.length
                    ? ''
                    : 'none')
            }}>
                <dt onClick={() => {
                    this.setState(prevState => ({
                        showCashier: !prevState.showCashier
                    }));
                    let [dd,
                        detailRealHeight] = [$('#about dd'), this.state.detailRealHeight];
                    if (!detailRealHeight) {
                        detailRealHeight = dd.height();
                        this.setState({detailRealHeight});
                    }
                    if (!this.state.showCashier) {
                        dd.css('height', 0);
                        $(dd).animate({
                            height: detailRealHeight
                        }, 300)
                    } else {
                        dd.css('height', detailRealHeight);
                        $(dd).animate({
                            height: 0
                        }, 300)
                    }
                }} className='rightSvg'>
                    <span>对应借款{this.state.showCashier}</span>
                    <Arrow fill='#c7c7cc'/>
                </dt>
                <dd ref='details' style={{
                    height: this.state.detailHeight,
                    display: (this.state.detailRealHeight
                        ? ''
                        : 'none')
                }}>
                    <ul>
                        {cashierPay}
                    </ul>
                </dd>
            </dl>
        </section>
    }
}

// ReactMixin.onClass(RecordDetail, Reflux.connect(store,'m'));
function select(state) {
    return {
        DETAIL: state.Responce.DETAIL || {}
    }
}
export default connect(select)(RecordDetail);
