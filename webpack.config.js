var webpack = require('webpack')
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        apps: './src/app/app.jsx',
        common: [
            // "react",
            "babel-polyfill",
            "react-dom",
            "react-redux",
            "redux",
            "react-motion",
            "react-router-dom",
            "dateformat",
            "jquery",
            "sweetalert2",
            "react-router-transition",
            "es6-promise",
            "./node_modules/sweetalert2/dist/sweetalert2.css"
        ]
    },

    // output: {
    //     path: path.resolve(__dirname, './dist'),
    //     filename: 'app.js',
    //     publicPath: '/',
    // },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].js', //最终打包生产的文件名
        publicPath: '/'
    },

    devServer: {
        historyApiFallback: true,
        noInfo: true,
        inline: true
    },

    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loader: 'babel-loader',

                query: {
                    presets: ['es2015', 'react']
                }
            }, {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /\.(css|scss)$/,
                loader: "style-loader!css-loader!postcss-loader!sass-loader"
            }
        ]
    },

    // devtool: 'hidden-source-map',
    devtool: 'eval',
    watch: true,
    performance: {
        hints: false
    },
    target: 'web',
    plugins: [
        // 开发环境配置
        new webpack.DefinePlugin({
            __LOCAL__: true, // 测试环境
            __PRO__: false, // 生产环境
        }),
        new HtmlWebpackPlugin({template: './index.html'}),
        new webpack.ProvidePlugin({swal: "sweetalert2", "$": "jquery", dateFormat:"dateformat"}),
        new webpack.optimize.CommonsChunkPlugin({names: ['common'], minChunks: Infinity}),
        new webpack.BannerPlugin("Copyright Jindw inc."),

    ]

}
