
## 测试id:1765416
## http://localhost:8080/#/?id=1765416

#米庄贷3.0接口对接文档

> 所有接口默认传`userKey`，接口结尾为`.json`

## 行政区划

> 省
>
> 接口:`/regions/provinces.json`
>
> type:`get`
>
> 请求参数:无
>
> 返回参数：
>
> | 变量名          | 含义   | 类型            | 备注               |      |
> | ------------ | ---- | ------------- | ---------------- | ---- |
> | provinceCode | 省份代码 | string        |                  |      |
> | provinces    | 省份列表 | array<object> | 请求成功时 返回一个或多个或零个 |      |
> | status       | 返回状态 | string        | 0请求成功，非0请求失败     |      |

**数据处理**

```jsx
const st = this.state.m;
let [provinces,cities,county] = [[],[],[]];
st.provinces.forEach((itm,ind)=>provinces.push(<dd key={`province_${ind}`} onClick={this.SelectCity.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));
```

> 市
>
> 接口：` /regions/cities.json`
>
> type:`get`
>
> 请求参数：
>
> ​                              provinceCode	   省份代码	       string	   必填
>
> 返回参数：
>
> | 变量名    | 含义        | 类型            | 备注           |      |
> | ------ | --------- | ------------- | ------------ | ---- |
> | cities | 返回该省的城市列表 | array<object> | 请求成功时 返回     |      |
> | status | 返回状态      | string        | 0请求成功，非0请求失败 |      |

 **数据处理**

```jsx
st.cities.forEach((itm,ind)=>cities.push(<dd key={`city_${ind}`} onClick={this.selectCounty.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));
```

> 区/县
>
> 接口：`/regions/districts.json`
>
> type:`get`
>
> 请求参数：cityCode	城市代码	   string	     必填
>
> 返回参数:
>
> | 变量名       | 含义           | 类型            | 备注           |      |
> | --------- | ------------ | ------------- | ------------ | ---- |
> | districts | 返回该城市的 县或区列表 | array<object> | 请求成功时 返回     |      |
> | status    | 返回状态         | string        | 0请求成功，非0请求失败 |      |

**数据处理**

```jsx
st.county.forEach((itm,ind)=>county.push(<dd key={`county_${ind}`} onClick={this.selectPlace.bind(this,itm)}>{itm.name}<Arrow fill='#3366cc'/></dd>));

```

> 实现方法
>
> 依赖：swiper3,react-modal

```jsx
        const sw =  new Swiper('.swiper-container',{
            swipeHandler : 'none',
            effect : 'coverflow',
            slidesPerView: 1,
            centeredSlides: true,
            coverflow: {
                rotate: 30,
                stretch: 10,
                depth: 60,
                modifier: 2,
                slideShadows : true
            },
            speed:200,
        });
```



```jsx
import ModalSelectPlace from 'react-modal'
////
<ModalSelectPlace
                    isOpen={this.state.modalPlaceIsOpen}
                    onAfterOpen={this.openModalPlace.bind(this)}
                    onRequestClose={this.closeModalPlace.bind(this)}
                    closeTimeoutMS={150}
                    style={{
                        overlay : {
                            top:$('header').height()
                        },
                        content : {
                            width:'100%',
                            height:'100%',
                            backgroundColor:'#eee',
                        }
                    }}
                >   
                    <div className="swiper-container selectCity">
                        <div ref='swiperWrapper' className="swiper-wrapper">
                            <div className="swiper-slide">
                                <dl className='rightSvg bankList citylist'>
                                    <dt onClick={this.closeModalPlace.bind(this)}><i></i>请选择地区<Arrow fill='#3366cc'/></dt>
                                    {provinces}
                                </dl>
                            </div>
                            <div className="swiper-slide">
                                <dl className='rightSvg bankList citylist'>
                                    <dt onClick={this.prevModalPlace.bind(this)}><i></i>请选择地区<Arrow fill='#3366cc'/>
                                        <span>{st.sProvince}</span>
                                    </dt>
                                    {cities}
                                </dl>
                            </div>
                            <div className="swiper-slide">
                                <dl className='rightSvg bankList citylist'>
                                    <dt onClick={this.prevModalPlace.bind(this)}><i></i>请选择地区<Arrow fill='#3366cc'/>
                                        <span>{st.sProvince} {st.sCity}</span>
                                    </dt>
                                    {county}
                                </dl>
                            </div>
                        </div>
                    </div>
                </ModalSelectPlace>
```


##账号系统

### 注册（#/regist）

> 首先验证是否已注册
>
> 接口：`/users/register/checkregisterable`
>
> type:`get`
>
> 参数：mobile(手机号码)

> 获取图形验证码
>
> 地址：`/validatecodes/randcode.json`
>
> 参数：mobile(手机号码)

> 发送短信验证码
>
> 接口：`/users/register/sendverifycode`
>
> type:`POST`
>
> 参数：`{mobile:手机号码,validateCode:图形验证码}`
>
> 返回参数：`status=0`为短信发送成功，否则返回失败原因

> 验证短信验证码
>
> 接口：`/users/register/checkvalidatecode`
>
> type:`POST`
>
> 参数：`{mobile:手机号码，validateCode:短信验证码}`
>
> 返回参数：`status =0`,验证码正确，否则验证码错误

> 设置登录密码
>
> 地址：`/users/register`
>
> type:`POST`
>
> 参数：`{mobile:手机号码,validateCode:图形验证码，password:密码}`
>
> 返回参数：`status=0`为成功，否则返回失败原因

### 找回密码(#/retrieve)

> 流程和注册几乎相同，最后一步为：
>
> 接口：`/users/resetpassword`
>
> type:`POST`
>
> 参数：`{mobile:手机号码,validateCode:图形验证码，password:密码}`
>
> 返回参数：`status=0`为成功，否则返回失败原因

### 登陆(#/login)

> 接口：`/users/login`
>
> type:`POST`
>
> 参数：`{mobile:手机号码,password:密码}`
>
> 返回参数：`status=0成功，否则返回失败原因

### 个人中心(#/personal)

> 接口：`/users`
>
> type:`GET`
>
> 参数：`{userkey}`
>
> 返回参数：`status=0成功，否则返回失败原因
>
> ​		返回用户数据

#### 个人中心详情(#/personal/me)

> 从上面参数获取userName，idCard

#### 银行卡信息(#/bank)

>接口：`/bankcards/user`
>
>type:`GET`
>
>参数：`{userkey}`
>
>响应：
>
>| 变量名        | 含义   | 类型     | 备注           |
>| ---------- | ---- | ------ | ------------ |
>| bankCard   |      | object |              |
>| bankCode   |      | string |              |
>| bankName   |      | string |              |
>| cardNumber |      | string |              |
>| id         |      | string |              |
>| idCard     |      | string |              |
>| status     |      | string |              |
>| userName   |      | string |              |
>| errorMsg   | 返回信息 | string |              |
>| status     | 返回状态 | number | 0请求成功，非0请求失败 |

### 账户安全(#/safety)

#### 修改登录密码(#/safety/login)

> 接口：`/users/modifypassword`
>
> type:`POST`
>
> 参数：
>
> | 变量名         | 含义   | 类型     | 备注    |
> | ----------- | ---- | ------ | ----- |
> | newPassword | 新密码  | string | 传的是密文 |
> | oldPassword | 老密码  | string | 传的是密文 |
> | userKey     | 用户键  | number |       |
> 响应：`status=0成功，否则失败`

#### 交易密码管理(#/safety/paid)

##### 验证是否设置过支付密码

> 接口：`/users/issetpaypassword`
>
> type:`GET`
>
> 参数：`{userkey}`
>
> 响应：`isSetPayPassword === true表示已经设置过了，如果没设置，会返回设置新密码用到的验证码`

**如果没设置过，则进入设置新支付密码**

##### 设置新支付密码(#/safety/newpaid)

> 接口：` /users/setpaypassword`
>
> type:`POST`
>
> 参数：
>
> | 变量名          | 含义   | 类型     | 备注   |
> | ------------ | ---- | ------ | ---- |
> | payPassword  | 支付密码 | string |      |
> | userKey      | 用户键  | number |      |
> | validateCode | 验证码  | string |      |
> 响应：`status=0表示成功`

##### 修改交易密码(#/safety/modifypaid)

> 接口：` /users/modifypaypassword`
>
> type:`POST`
>
> 参数：
>
> | 变量名         | 含义   | 类型     | 备注   |
> | ----------- | ---- | ------ | ---- |
> | newPassword | 新密码  | string |      |
> | oldPassword | 旧密码  | string |      |
> | userKey     | 用户   | string |      |
> 响应：`status=0表示成功`

##### 找回交易密码(#/safety/findpaid)

**首先验证实名信息是否正确**

> 接口：` /users/checkrealauth`
>
> type:`POST`
>
> 参数：
>
> | 变量名     | 含义      | 类型     | 备注   |
> | ------- | ------- | ------ | ---- |
> | idCard  | 用户身份证号  | string |      |
> | name    | 用户身份证姓名 | string |      |
> | userKey | 用户key   | string |      |
> 响应：`status=0表示成功`

**第二步短信验证码，接口同注册短信验证码接口，获取验证码**

第三步找回密码，同设置新的交易密码

## 首页(#/)

### 首页数据

> 接口:`/users/home`
>
> type:`GET`
>
> 参数：`{userkey}`
>
> 返回参数：
>
> |      |                              |        |        |      |
> | ---- | ---------------------------- | ------ | ------ | ---- |
> |      | 变量名                          | 含义     | 类型     | 备注   |
> |      | avaiAmount                   | 可用额度   | number |      |
> |      | currentShouldRepaymentAmount | 当前应还款额 | number |      |
> |      | errorMsg                     | 请求结果描述 | string |      |
> |      | quotaAmount                  | 总授信额度  | number |      |
> |      | status                       | 请求结果   | number | 0成功  |
> |      | totalShouldRepaymentAmount   | 全部应还款额 | number |      |

### 是否可以借款

> 接口`/users/applystatus`
>
> type:`POST`
>
> 参数：`{userkey}`
>
> 返回参数：`status=0可以借款`

##提额(#/raise)

### 基础授信认证状态

> 接口`/userauth`
>
> type:`GET`
>
> 参数：`{userkey}`
>
> 返回参数：
>
> ​	REALAUTH：实名认证
>
> ​	BINDCARDAUTH：银行卡认证
>
> ​	CONTACTSAUTH：联系人认证
>
> ​	MERCHANTAUTH：商户资质证明
>
> ​	MERCHANTRENTAUTH：经营性房屋合同
>
> status为1代表已提交，否提未提交

### 信用提额认证状态

> 接口`/userextauths`
>
> type:`GET`
>
> 参数：`{userkey}`
>
> 返回参数：
>
> ​	PBCAUTH：人行征信
>
> ​	MARITALAUTH：婚姻状况
>
> ​	LIVEAUTH：居住地信息
>
> ​	CENSUSAUTH：户籍证明
>
> status为1代表已提交，否提未提交

### 认证审核状态

> 接口：`/users/applystatus`
>
> type:`POST`
>
> 参数：`{userkey}`
>
> 响应：status!=0表示未提交，否则返回各种状态



## 基础提额(#/raise)

### 实名认证(#/raise/certification)

> 提交接口` /userauth/realnameauth`
>
> type:`POST`
>
> 参数：
>
> |      | 变量名            | 含义             | 类型     | 备注   |
> | ---- | -------------- | -------------- | ------ | ---- |
> |      | idCard         | 用户身份证号         | string | 必填   |
> |      | idCardBackUrl  | 身份证反面图片地址      | string |      |
> |      | idCardFrontUrl | 身份证正面图片地址      | string |      |
> |      | idCardHandUrl  | 手持身份证图片地址      | string |      |
> |      | realName       | 用户实名           | string | 必填   |
> |      | userKey        | 用户userKey，唯一标识 | string | 必填   |
> 返回参数：
>
> |      | 变量名                   | 含义       | 类型      | 备注                          |
> | ---- | --------------------- | -------- | ------- | --------------------------- |
> |      | errorMsg              | 返回信息     | string  |                             |
> |      | realNameAuthFailCount | 实名认证失败次数 | number  |                             |
> |      | result                | 实名认证结果   | boolean | true：一致，false：不一致，null：无法认证 |
> |      | status                | 返回状态     | string  | 0请求成功，非0请求失败                |

### 银行卡认证(#/raise/bank)

#### 获取银行卡列表

> 接口` /bankinfos`
>
> type:`GET`
>
> 参数：`{userkey}`
>
> 返回参数：
>
> |      | 变量名               | 含义   | 类型            | 备注           |
> | ---- | ----------------- | ---- | ------------- | ------------ |
> |      | bankInfos         |      | array<object> |              |
> |      | bankCode          |      | string        |              |
> |      | bankDayLimit      |      | number        |              |
> |      | bankMonthLimit    |      | number        |              |
> |      | bankName          |      | string        |              |
> |      | bankSingleLimit   |      | number        |              |
> |      | cardNumber        |      |               |              |
> |      | dayLimit          |      | number        |              |
> |      | isMaintain        |      | number        |              |
> |      | isVirtual         |      | number        |              |
> |      | maintainEndTime   |      |               |              |
> |      | maintainStartTime |      |               |              |
> |      | monthLimit        |      | number        |              |
> |      | payGateBankCode   |      | string        |              |
> |      | payGateType       |      | string        |              |
> |      | singleLimit       |      | number        |              |
> |      | status            |      | string        | 0请求成功，非0请求失败 |

#### 发送绑卡短信验证码

> 接口:`/bankcards/sendbindvalidatecode`
>
> type:`POST`
>
> 参数：
>
> | 变量名        | 含义      | 类型     | 备注   |
> | ---------- | ------- | ------ | ---- |
> | bankCode   | 银行编码    | string |      |
> | cardNumber | 银行卡号    | string |      |
> | mobile     | 银行预留手机号 | string |      |
> | userKey    | 用户登录键   | string |      |
> 返回参数：
>
> | 变量名         | 含义      | 类型     | 备注           |
> | ----------- | ------- | ------ | ------------ |
> | bankCardId  | 绑定银行卡Id | number |              |
> | errorMsg    | 请求结果描述  | string |              |
> | payGateType | 支付网关编码  | string |              |
> | status      | 请求结果    | number | 0请求成功，非0请求失败 |

#### 绑卡

> 接口：`/bankcards/confirm`
>
> type:`POST`
>
> 参数：
>
> | 变量名          | 含义    | 类型     | 备注          |
> | ------------ | ----- | ------ | ----------- |
> | bankCardId   | 银行卡id | string |             |
> | mobile       | 手机号   | string |             |
> | payGateType  | 支付网关  | string | 例子如 BAO_FOO |
> | userKey      | 用户键   | string |             |
> | validateCode | 短信验证码 | string |             |
> 响应：
>
> | 变量名        |  含义  |   类型    | 备注                 |
> | ---------- | :--: | :-----: | ------------------ |
> | bindResult | 绑卡结果 | boolean | 绑卡是否成功的布尔值，true为成功 |
> | errorMsg   | 结果描述 | string  |                    |
> | status     | 返回状态 | string  | 0请求成功，非0请求失败       |

### 添加联系人(#/raise/contact)

> 接口：`/contacts`
>
> type:`POST`
>
> 参数：
>
> | 变量名      | 含义           |   类型   | 备注                                       |      |
> | :------- | :----------- | :----: | :--------------------------------------- | ---- |
> | contacts | 联系人列表，JSON格式 | string | [ { "mobile": "13333333333", "name": "张三", "relationship": "父母" }, { "mobile": "13333333332", "name": "李西", "relationship": "父母" } ] |      |
> | userKey  | 用户键          | string |                                          |      |

### 商户资质证明(#/raise/commercial)

> 接口：`/companies/auth`
>
> type:`POST`
>
> 参数：
>
> | 变量名                | 含义             | 类型     | 备注   |
> | ------------------ | -------------- | ------ | ---- |
> | address            | 详细地址           | string |      |
> | businessLicence    | 营业执照注册号        | string |      |
> | businessLicenceUrl | 营业执照图片URL      | string |      |
> | regionCode         | 企业所在地区编码       | string |      |
> | userKey            | 用户userKey，唯一标识 | string |      |
> 响应 ：
>
> | 变量名      | 含义   | 类型     | 备注           |
> | -------- | ---- | ------ | ------------ |
> | errorMsg | 返回信息 | string |              |
> | status   | 返回状态 | string | 0请求成功，非0请求失败 |

### 经营性房屋合同(#/raise/pact)

> 接口：` /housecontfaractpics/auth`
>
> type:`POST`
>
> 参数：
>
> | 变量名     | 含义             | 类型            | 备注   |
> | ------- | -------------- | ------------- | ---- |
> | picUrls | 经营性房屋合同照片URL列表 | array<string> |      |
> | userKey | 用户键            | string        |      |
> 响应：
>
> | 变量名      | 含义     | 类型     | 备注           |
> | -------- | ------ | ------ | ------------ |
> | errorMsg | 请求结果描述 | string |              |
> | status   | 请求结果   | number | 0请求成功，非0请求失败 |

## 信用提额(#/raise)

### 人行征信(#/raise/credit)

> 接口：` /userextauths/pbc`
>
> type:`POST`
>
> 参数：
>
> | 变量名          | 含义    | 类型     | 备注   |
> | ------------ | ----- | ------ | ---- |
> | password     | 用户密码  | string |      |
> | userKey      | 用户信息  | string |      |
> | userName     | 用户名   | string |      |
> | validateCode | 图形验证码 | string |      |
> | verifyCode   | 人行验证码 | string |      |
> 响应：
>
> | 变量名      | 含义   | 类型     | 备注   |
> | -------- | ---- | ------ | ---- |
> | errorMsg | 返回消息 | string |      |
> | status   | 状态   | string |      |

### 婚姻状况认证(#/raise/marry)

> 接口：`/userextauths/marital`
>
> type:`POST`
>
> 参数：
>
> | 变量名        | 含义     | 类型     | 备注   |
> | ---------- | ------ | ------ | ---- |
> | filePath   | 婚姻状况图片 | string |      |
> | mariStatus | 婚姻状况   | string |      |
> | userKey    | 用户登录信息 | string |      |
> 响应：
>
> | 变量名      | 含义   | 类型   | 备注   |
> | -------- | ---- | ---- | ---- |
> | errorMsg | 返回消息 |      |      |
> | status   | 状态   |      |      |

### 居住信息认证(#/raise/live)

> 接口：`/userextauths/live`
>
> type:`POST`
>
> 参数：
>
> | 变量名      | 含义   | 类型     | 备注   |
> | -------- | ---- | ------ | ---- |
> | address  | 地址   | string |      |
> | cityCode | 地方   | string |      |
> | filePath | 图片   | string |      |
> | userKey  |      | string |      |
> 响应：`status=0表示成功，否则返回失败`

### 户籍证明(#/raise/census)

> 接口：` /userextauths/census`
>
> type:`POST`
>
> 参数：
>
> | 变量名      | 含义   | 类型     | 备注   |
> | -------- | ---- | ------ | ---- |
> | filePath | 文件路径 | string |      |
> | userKey  |      | string |      |
> 响应：`status=0表示成功，否则返回失败`

## 申请借款(#/loan)

### 借款用途列表

> 接口：` /loans/loanpurposes`
>
> type:`GET`
>
> 参数：无
>
> 响应：`loanPurposes->id,name`

### 借款方式列表（期限）

> 接口：`/loans/repaymentplans`
>
> type:`GET`
>
> 参数：`userKey`
>
> 响应：`repaymentPlans->id,loanPlanName：名字，loanCycle(周期)，loanRatio：利率，termType`：类型

### 计算申请借款利息等信息

>接口：`/loans/calculateapplyloan`
>
>type:`GET`
>
>参数：
>
>| 变量名             | 含义     | 类型     | 备注   |
>| --------------- | ------ | ------ | ---- |
>| loanAmount      | 借款金额   | string |      |
>| repaymentPlanId | 还款方案Id | number |      |
>| userKey         | 用户Key  | string |      |
>响应：
>
>| 变量名                 | 含义         | 类型     | 备注           |
>| ------------------- | ---------- | ------ | ------------ |
>| errorMsg            | 请求结果描述     | string |              |
>| fee                 | 手续费        | number |              |
>| installment         | 每期还款金额（本息） | number | 分期借款返回，全款不返回 |
>| installmentInterest | 每期还款利息     | number | 分期借款返回，全款不返回 |
>| realPayAmount       | 实际到款金额     | number |              |
>| repaymentAmount     | 还款金额（本息）   | number |              |
>| status              | 请求结果       | number | 0请求成功，非0请求失败 |

### 计算还款计划明细

> 接口：`/loans/calculaterepayments`
>
> type:`GET`
>
> 参数：
>
> | 变量名        | 含义     | 类型     | 备注   |
> | ---------- | ------ | ------ | ---- |
> | loanAmount | 借款金额   | number |      |
> | loanCycle  | 借款期数   | number |      |
> | rate       | 借款利率   | number |      |
> | termType   | 计息周期类型 | string |      |
> 响应：
>
> | repayments | 还款计划明细    | array<object> |        |      |
> | ---------- | --------- | ------------- | ------ | ---- |
> |            | amount    | 还款金额          | number |      |
> |            | interest  | 利息            | number |      |
> |            | number    | 第几期           | number |      |
> |            | principal | 本金            | number |      |

### 确认申请借款(#/loan/confirm)

> 接口：`/loans/apply`
>
> type:`POST`
>
> 参数：
>
> | 变量名             | 含义      | 类型     | 备注   |
> | --------------- | ------- | ------ | ---- |
> | amount          | 借款金额    | number |      |
> | bankCardId      | 收款银行卡Id | number |      |
> | payPassword     | 支付密码    | string |      |
> | purposeId       | 借款用途Id  | number |      |
> | repaymentPlanId | 还款方案id  | number |      |
> | userKey         | 用户键     | string |      |
> 响应：`status=0则成功`
