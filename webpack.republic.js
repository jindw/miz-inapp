var webpack = require('webpack')
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        apps: './src/app/app.jsx',
        common: [
            // "react",
            "babel-polyfill",
            "react-dom",
            "react-redux",
            "redux",
            "react-motion",
            "react-router-dom",
            "dateformat",
            "jquery",
            "sweetalert2",
            "react-router-transition",
            // "es6-promise",
            "./node_modules/sweetalert2/dist/sweetalert2.css"
        ]
    },

    // output: {
    //     path: path.resolve(__dirname, './dist'),
    //     filename: 'app.min.js',
    //     publicPath: '/',
    // },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].min.js', //最终打包生产的文件名
        publicPath: 'https://cdn.mizlicai.com/mizdai/inapp-2.0/',
    },

    devServer: {
        historyApiFallback: true,
        noInfo: true,
        inline: true
    },

    module: {
        rules: [
            {
                test: /\.(jsx|js)?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',

                query: {
                    presets: ['es2015', 'react']
                }
            }, {
                test: /\.(css|scss)$/,
                loader: "style-loader!css-loader!postcss-loader!sass-loader"
            }
        ]
    },

    // devtool: 'hidden-source-map',
    devtool: 'nosources-source-map',

    watch: true,

    // performance: {
    //     hints: false
    // },
    plugins: [
        // 开发环境配置
        new webpack.DefinePlugin({
            __LOCAL__: false, // 测试环境
            __PRO__: true, // 生产环境
            'process.env': {
                'NODE_ENV': '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: false
            }
        }),
        new HtmlWebpackPlugin({template: './index.html'}),
        new webpack.ProvidePlugin({swal: "sweetalert2", "$": "jquery"}),
        new webpack.optimize.CommonsChunkPlugin({names: ['common'], minChunks: Infinity}),
        new webpack.BannerPlugin("Copyright Jindw inc."),
    ]

}
